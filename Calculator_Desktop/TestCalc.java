
import calculator.FXMLDocumentController;
import org.junit.Assert;
import org.junit.Test;

import static java.lang.Double.sum;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Vlad)))
 */

public class TestCalc {
    
  @Test
  public void testSum(){
      int firstNum=5;
      int secondNum=7;
      Assert.assertEquals(firstNum+secondNum,FXMLDocumentController.sum(firstNum,secondNum),0.1);
           
  }  
    
  
   @Test
  public void testSubstract(){
      int firstNum=5;
      int secondNum=7;
      Assert.assertEquals(firstNum-secondNum,FXMLDocumentController.subtract(firstNum,secondNum),0.1);
      
      
      
  }
  
   @Test
  public void testMult(){
      int firstNum=5;
      int secondNum=7;
      Assert.assertEquals(firstNum*secondNum,FXMLDocumentController.mult(firstNum,secondNum),0.1);
      
      
      
  }
  
   @Test
  public void testdivision(){
      float firstNum=5;
      float secondNum=7;
      Assert.assertEquals(firstNum/secondNum,FXMLDocumentController.division(firstNum,secondNum),0.1);
      
      
      
  }


    
}
