package calculator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXMLDocumentController {

    @FXML
    private Button button;

    @FXML
    private TextField text1;

    @FXML
    private TextField text2;

    @FXML
    private TextField text3;

    @FXML
    private Label text4;

    @FXML
    void handleButtonAction(ActionEvent event) {
        try{
            float number1 = Float.parseFloat(text1.getText());
            float number2 = Float.parseFloat(text2.getText());
            String action = text3.getText();
            float result=0;


            switch(action)
            {
                case "+":
                    result=number1+number2;
                    text4.setText(Float.toString(sum(number1,number2)));
                    break;
                case "-":
                    result=number1-number2;
                    text4.setText(Float.toString(subtract(number1,number2)));
                    break;
                case "*":
                    result=number1*number2;
                    text4.setText(Float.toString(mult(number1,number2)));
                    break;
                case "/":
                    if(number2==0)
                        result=number1/number2;
                    text4.setText(Float.toString(division(number1,number2)));
                    break;
                default:
                    text4.setText("Action:+,-,*,/");
                    break;

            }

        }
        catch(Exception e){
            text4.setText("Use only numbers");
        }
    }


    public static float sum(float num1,float num2){
        return num1+num2;
    }


    public  static float subtract(float num1,float num2){
        return num1-num2;
    }


    public static  float mult(float num1,float num2){
        return num1*num2;
    }


    public static float division(float num1,float num2){
        return num1/num2;
    }


}