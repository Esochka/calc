package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public  void onButtonClick(View v){
        EditText num1 = (EditText)findViewById(R.id.num1);
        EditText num2 = (EditText)findViewById(R.id.num2);
        int number1 = Integer.parseInt(num1.getText().toString());
        int number2 = Integer.parseInt(num2.getText().toString());
        EditText ac = (EditText)findViewById(R.id.act);
        EditText res = (EditText)findViewById(R.id.res);
        String action = ac.getText().toString();
        float r;
        switch(action)
        {
            case "+":
                r=number1+number2;
                res.setText(Float.toString(r));
                break;
            case "-":
                r=number1-number2;
                res.setText(Float.toString(r));
                break;
            case "*":
                r=number1*number2;
                res.setText(Float.toString(r));
                break;
            case "/":
                if(number2==0){
                    res.setText("Dont use 0");
                }else {
                    r = (float) number1 / number2;
                    res.setText(Float.toString(r));
                }
                break;
            default:
                res.setText("Wrong data");

        }

    }
}